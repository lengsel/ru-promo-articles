# Kalendar v0.4.0

_Автор:_ [Claudio Cambra](https://claudiocambra.com/)

I know it has been a while since our last update, but I can assure you that we have not been twiddling our thumbs! For the holiday season, we are happy to bring a new release of Kalendar, which we have worked hard on to bring new features, better performance, and a bunch of bugfixes that should make Kalendar better than ever on those of you with shiny new machines. This release should land over the newt couple of days.

Happy holidays!

*Note: Kalendar is still under heavy development. You’re free to poke around and try it out, but it is not yet final software! If you want to contribute to its development, join us in [Kalendar’s Matrix room.](https://matrix.to/#/#kalendar:kde.org)*

## Our 0.4.0 release

We are excited to have you try Kalendar, and we want your feedback — especially bug reports! These will help us improve Kalendar as much as possible before releasing 1.0.

**It is now in the hands of distribution packagers to add Kalendar to their repositories.** The most up-to-date and unstable version of Kalendar will continue to come from our git repository, and some users have gone ahead and started packaging builds of Kalendar coming straight from our master branch.

Git builds:

* [openSUSE has a package on OBS](https://build.opensuse.org/package/show/KDE:Unstable:Extra/kalendar) (KDE:Unstable:Extra)
* Fedora has two COPR packages [(1)](https://copr.fedorainfracloud.org/coprs/zawertun/kde/package/kalendar/), [(2)](https://copr.fedorainfracloud.org/coprs/marcdeop/kalendar/)
* [Arch has an AUR package](https://aur.archlinux.org/packages/kalendar-git/)
* [Neon has Kalendar packaged in Unstable](https://build.neon.kde.org/view/3%20unstable%20%E2%98%A3%20git%20master/job/focal_unstable_neon-packaging_kalendar/)

[Stable versions are now also available on a number of distributions, such as Alpine, Fedora, Neon User, Manjaro, and NixOS!](https://repology.org/project/kalendar/versions)

Now, here’s what’s new this week:

## Three-day and single-day views

0.4.0 brings two new views: the three-day and single-day views. These are based on the week view, presenting events and tasks according to their times. These new views should make it much easier to check your calendars when the window is width-constrained, or when you have lots of overlapping events.

![0](https://claudiocambra.com/wp-content/uploads/2021/12/image.png)

The three-day view therefore replaces the week view on mobile. The week view was borderline unusable on mobile, and the three-day view makes for a much more usable calendar view.

<https://claudiocambra.com/wp-content/uploads/2021/12/simplescreenrecorder-2021-12-03_17.32.38.mp4>

The day view is also accessible as an overlay across all of Kalendar’s views, on both desktop and mobile. Clicking on the day header, in the month, week, and three-day views brings up the day view for that specific day.

## Drag-and-drop calendar changing

<https://claudiocambra.com/wp-content/uploads/2021/12/simplescreenrecorder-2021-12-24_01.05.26.mp4>

You can now drag tasks from the tasks view onto a calendar in the sidebar and quickly change a task’s calendar. We are working to extend this new feature to other views for the next release.

## Making Kalendar faster than ever

This past week I finally received my Pinephone Pro, and was immediately disappointed to find that Kalendar was still somewhat sluggish on the device. Over the new year, we will be making sure to continue refining and optimizing Kalendar to make it as smooth as we can on the device.

We have already begun this work. In the week view, changing the view’s date to far-flung dates should no longer cause Kalendar to freeze while it pointlessly processes new dates. This should make navigation significantly faster.

We have also tweaked what components are loaded synchronously and which ones are loaded asynchronously. This tuning should help Kalendar more quickly load what you need, while loading things that you may not need in the background. Overall, this should make the app feel smoother and faster.

## Usability improvements

![3](https://claudiocambra.com/wp-content/uploads/2021/12/image-3.png)

A nice new change in the week, three-day and day views is that your scroll position is conserved across the different weeks/three-day/day intervals. This should save you from having to always scroll to the times that you are interested in viewing in these views.

We have also ensured that now every dialog window in Kalendar can be closed with the Esc key. Those of you on very up-to-date distributions (e.g. Neon user, Arch, openSUSE Tumbleweed, etc.) will have likely already experienced this change thanks to changes upstream, but changes within Kalendar should ensure that the Esc key closes dialog windows regardless of the system’s Kirigami version.

The tasks view now also shows the date always according to your system locale.

Lastly, trying to start a new instance of Kalendar when the application is already open will bring the Kalendar window to the front of your desktop!

## Big improvements to the alarm notifier client

![4](https://claudiocambra.com/wp-content/uploads/2021/12/image-2.png)

Thanks to Volker, Kalendar’s notifier daemon has acquired some big technical improvements. Eventually, this daemon will be used by both Kalendar and KOrganizer, so these changes will eventually benefit KOrganizer users too!

The first improvement is that the daemon is much smaller and more efficient, retaining the same functionality. Additionally, the alarm client is now more reliable.

The notification daemon now also checks for alarms according to the stored alarm times, rather than polling every minute for active alarms. This is significantly more efficient (which should help save your battery!) and also more reliable across restarts.

We have also removed the system tray entry. We found that there weren't enough use cases to justify its retention. For one, the system tray is inaccessible on Plasma Mobile and on GNOME, making the tray icon unusable. Even on Plasma, we found that there weren't many reasons to use it; since we use KNotifications, we can simply use Plasma’s Do Not Disturb mode to hide notifications, and we found the use-case of turning off the daemon rare enough that it didn’t make much sense to keep around.

## Other bug-fixes and small changes

* [Fixed current time indicator in three-day and day view](https://invent.kde.org/pim/kalendar/-/commit/e8feeb107763a5caa2a5d2a836553ed85590748d) (Claudio Cambra)
* [Strip carriage returns from new tag names and add max number of characters](https://invent.kde.org/pim/kalendar/-/commit/21902bd4e0bbfd511780fb5b54685a9ccc7e3d49) (Claudio Cambra)
* [Fixed ‘Today’ button in all hourly views](https://invent.kde.org/pim/kalendar/-/commit/567b42636170aa516730b79ea9d4497cca4c29aa) (Claudio Cambra)

## Supporting us

Is there anything you’d like to see added to Kalendar? Get in touch! I’m @clau-cambra:kde.org on Matrix.

If you want to support Kalendar’s development, I strongly encourage you to donate to the KDE community. These donations help us keep our infrastructure running, including our GitLab instance, our websites, and more. You can donate at <https://kde.org/community/donations/>.

If you are already a KDE supporter and want to support Kalendar development directly, you can donate to Claudio Cambra (Kalendar maintainer and… oh, that’s me! 👋) on ([Liberapay](https://liberapay.com/claucambra/)).

_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://claudiocambra.com/2021/12/25/happy-holidays-kalendar-v0-4-0-is-our-gift-and-it-brings-new-views-improved-performance-and-many-many-bugfixes-kalendar-devlog-24/  

<!-- 💡: daemon → служба -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: upstream → исходный репозиторий -->
