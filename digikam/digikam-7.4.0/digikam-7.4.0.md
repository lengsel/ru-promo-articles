# digiKam 7.4.0 is released

![0](https://i.imgur.com/9k5o5TI.png)

*digiKam 7.4.0 running under Apple MacOS*

Dear digiKam fans and users,

After five months of maintenance, the digiKam team is proud to present version 7.4.0 of its open source digital photo manager. This new version arrives [after 20 years of development](https://www.digikam.org/news/2021-12-11_digikam_20_anniversary/) since the first public 0.1.0, released at Christmas 2001.

Here is the list of most important features coming with this release:

## Showfoto Folder-View and Stack-View

Showfoto, the standalone version of digiKam image editor, comes with a new left sidebar including two tabs to navigate in your local file system and to manage your preferred stack of images to load in application.

![1](https://i.imgur.com/Apa6TeO.png)

The Folder-View is a standard navigation view over the local file system. It allows:

* To filter images by type-mimes,
* Render thumbnails with different sizes,
* Render file list with details,
* Show photograph metadata on tool-tips,
* Play over the navigation history,
* Save your preferred places in Bookmarks,
* Give access to the usual places from your computer,
* Load folder contents to the editor using Stack-View,
* Visualize quickly the images using Slideshow plugins.

![2](https://i.imgur.com/7QteLSp.png)

The Stack-View is a flat list of grouped images. It allows:

* Load quickly items in editor,
* Manage list of files to host in editor thumb-bar,
* Allow hosting stacked files from different folders,
* Render thumbnails with different sizes,
* Show photograph metadata on tool-tips,
* Save your preferred list of files in hierarchized Favorites tree-view,
* Edit and manage Favorites properties,
* Search over Favorites tree-view,
* Visualize quickly the stacked items using Slideshow plugins.

Remember that Showfoto does not use a database as digiKam. All settings and properties managed by the Folder-View and the Stack-View are stored in a simple XML config file. The new left sidebar will allow you to explore images stored on your disk and manage a small collection of your preferred items.

## Linux, macOS, and Windows Bundle Improvements

![3](https://i.imgur.com/pIyr594.png)

The macOS package is now [compatible with the last Apple Monterrey](https://bugs.kde.org/show_bug.cgi?id=444592) operating system. The AppImage bundle is now compiled with [Linux Mageia 7.1](https://blog.mageia.org/en/2019/07/16/mageia-7-1-mageia-7-with-ryzen-3000-hardware-support/) for a better binary compatibility with recent distributions. The last frameworks Qt 5.15.2, KF5 5.84, and [Exiv2 0.27.5](https://github.com/Exiv2/exiv2/releases/tag/v0.27.5) are now used everywhere.

![4](https://i.imgur.com/eMIcTEn.jpg)

Internal RAW engine have been also updated to last [Libraw snapshot 202110](https://www.libraw.org/news/libraw-202110-snapshot) and introduce these supports:

* Sony Lossless compressed support
* Panasonic v6/12 bit,
* DJI Mavic Air 2S,
* Fujifilm GFX 50S II, GFX 100S, X-E4,
* GoPro HERO9, HERO10,
* Nikon Z fc,
* Panasonic GH5 Mark II,
* Pentax K3 Mark III,
* Olympus E-P7,
* Ricoh GR IIIx,
* Sony A1, A7R-IIIA (ILCE-7RM3A), A7R-IVA (ILCE-7RM4A), ZV-E10,
* Canon EOS M50 Mark II.

We plan to use the new [Qt 6 framework](https://www.qt.io/product/qt6) everywhere as it becomes available in standard and all Qt based dependencies are now mostly ported and ready to use. For macOS support, we plan to compile the application as [Universal Binary](https://en.wikipedia.org/wiki/Universal_binary) to run natively on Apple Silicon Architecture, as current version is only targeted for Intel but work fine with [Rosetta 2](https://en.wikipedia.org/wiki/Rosetta_%28software%29#Rosetta_2) for M1 based computers.

## New Tool to Share with MJPEG Stream

A new plugin was developed and tested to export photos as [Motion JPEG Stream](https://en.wikipedia.org/wiki/Motion_JPEG).

This tool allows users to share items on the local network through a MJPEG Stream server. Items to share can be selected one by one or by group through a selection of albums.

Motion JPEG is a video compression format in which each video frame or interlaced field of a digital video sequence is compressed separately as a JPEG image. MJPEG streams is a standard which allows network clients to be connected without additional module. Most major web browsers and players support MJPEG streams.

Stream can be optimized with image sizes and JPEG quality to reduce the network bandwidth used. Users can customize the delay and the transition between images. A visual effect can be applied to the image while rendering on the web browser. Finally, an On Screen Display overlay can be shown to print image properties.

To access the stream from your browser, use http://address:port as URL, with address the MJPEG address, and port the MJPEG port set in config dialog. More than one computer can be connected to the MJPEG server at the same time.

![5](https://i.imgur.com/92D1JdG.png)

## Local Thumbnails Database Supports

A long time feature was to allow users to store the thumbnails database in a local file system when a remote MySQL/MariaDB database is used with digiKam, for example with a NAS connected to the local network. The bandwidth uses is very important in this case to share Wavelets compressed thumbnails between the remote database and the digiKam session. Using a local SQLite database hosted on the host computer will solve this issue and increase the application performances while browsing album contents.

![6](https://i.imgur.com/GzMtyvI.png)

## Image Quality Sorter Improvements and Focus Area Viewer

![7](https://i.imgur.com/NsewzzW.png)

This summer, one student named Phuoc-Khanh Le has works to [improve the Image Quality Sorter algorithms](https://community.kde.org/GSoC/2021/Ideas#Project:_Improve_Image_Quality_Sorter_algorithms)

The Image Quality Sorted tool from digiKam is used to sort images by quality and register items with flags in the database for future post-processing. Analyzed image qualities are noise, focus, exposure, and compression. Algorithms used in the background, written a few years ago do not give expected results in all shot conditions and need to be re-written for performances, especially using OpenCV framework.

![8](https://i.imgur.com/YsoQOLo.png)

A new feature implemented in this project is the capability to [extract the focus zone](https://bugs.kde.org/show_bug.cgi?id=138704) used by the camera to identify the area where the subject of the photography is located, and to check if the quality of image matches with the user criteria. For the moment, few camera models are supported:

* Canon: EOS, 7D Mark ii, 5D Mark iii, 5D Mark iv, 350D, 40D, 50D, 60D, 70D, 80D, 7D, 5D, G12, G16, G1X, G5X, IXUS 310 HS, SX30, SX40 HS,
* Nikon: D7100, D7200, D700, D800, D810, D800E, D5500, D5300, D5200,
* Sony: α7 III, α7R II, α7R III, α7R IV, α9, α9 II, α6100, α6400, α6500, α6600,
* Panasonic.

We plan to add more devices in the future by the use of the powerful ExifTool metadata parser. The AF Focus area used by the camera can be visualized over a photo while previewing contents. This feature is also [available in Light Table to easy compare shots with the best focus area selected.

![9](https://i.imgur.com/jbNbE1W.jpg)

This project has been merged and included in this 7.4.0 release. Student has written a [long report](https://community.kde.org/GSoC/2021/StatusReports/PhuocKhanhLE) about all changes and tests performed.

![10](https://i.imgur.com/XfjzDRK.png)

## Final Words

As you can see, digiKam version 7.4.0 has a lot going for it. [The Bugzilla entries closed](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=7.4.0) alone for this release are impressive, with more than 200 files closed this autumn.

Thanks to all users for [your support and donations](https://www.digikam.org/donate/), and to all contributors, students, testers who allowed us to improve this release.

digiKam 7.4.0 source code tarball, Linux 64 bits AppImage bundles, macOS Intel package, and Windows 64 bits installers can be downloaded from [this repository](https://download.kde.org/stable/digikam/7.4.0/).

Rendez-vous in spring 2022 for the next digiKam 7.5.0 release.

Happy digiKaming, Merry Christmas, and Best Wishes for 2022.

_Автор:_ Разработчики digiKam  
_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://www.digikam.org/news/2021-12-17-7.4.0_release_announcement/  

<!-- 💡: thumbnail → миниатюра -->
