# GCompris Releases Version 2.0

![0](https://dot.kde.org/sites/dot.kde.org/files/r2.0.png)

*Tux, Konqi and friends bring you new fun GCompris activities these holidays.*

**[GCompris](https://www.gcompris.net) 2.0 improves classic favorites loved by children of all ages, and adds even more activities for more fun and learning.**

**[GCompris](https://www.gcompris.net)** is KDE's educational suite of more than 170 activities and pedagogical games. It is used by teachers, parents and, most importantly, children worldwide. GCompris is a fixture in classrooms and at home, giving kids the opportunity to practice a wide range of skills while having fun at the same time.

![1](https://dot.kde.org/sites/dot.kde.org/files/main_screen.png)

*GCompris comes with more than 170 activities.*

GCompris 2.0's educational value and the hours of fun and entertainment it provides children of all ages makes it the perfect present for these festive holidays.

Let's see some of the new and fun activities you will find in GCompris:

**Baby Mouse** is for children learning to use a computer for the first time. It presents them with a friendly environment with brightly colored ducks where they can use a mouse, a touchscreen or any other input device to move a duck, click on a blank part of the screen, or click on other elements in the screen and receive visual and audio feedback. This activity is great to help develop hand-eye coordination and dexterity.

With **Ordering numbers** and **Ordering letters**, children familiar with numbers and the alphabet can practice ordering within several ranges. A step up is **Ordering sentences**, in which kids can practice reading and grammar by sorting out the parts of sentences.

![2](https://dot.kde.org/sites/dot.kde.org/files/ordering_sentence.png)

*Children can practice ordering numbers, letters and sentences at different levels.*

In **Positions**, another activity that helps children practice reading comprehension and spatial location at the same time, the player sees pictures of a child and a box and has to choose the word that best describes where they are relative to each other.

Getting back to numeracy activities, GCompris 2.0 includes a wide range of activities that mimic basic manipulation math games, allowing young players to experiment with elements, grouping them in sets of up to ten items. This helps them build a clear concept of the decimal system, and, as with many GCompris activities, an educator can gradually increase the difficulty level, allowing the activities to be used with children of ages between 3 and 10. Once they grasp the concept of the decimal system, the addition and subtraction activities, also based on math manipulation, help practice arithmetic.

Along with other classics, like chess, align four, and checkers, fans of strategy games will enjoy **Oware**, a game that requires forethought and, again, numeracy skills. Oware is originally a traditional African pastime and can be played against a friend or against Tux, offering unlimited hours of fun.

![3](https://dot.kde.org/sites/dot.kde.org/files/oware_1600_1000.png)

*Oware is a traditional African strategy game that requires forethought and numeracy skills.*

Talking about discovering things from around the world, let's not forget our the young globetrotters! For them, GCompris includes many reworked and new maps for them to explore and discover new countries. Not only will they learn the location of nations and regions, but they will also be able to visit and learn about famous locations and monuments.

Another way of finding your way around is by following directions. The four **Path** activities, *Path encoding absolute*, *Path encoding relative*, *Path decoding absolute*, and *Path decoding relative*, help kids learn to interpret abstract directions. Using arrows that can indicate a direction on a map (up, down, left, or right) or relative to Tux's orientation (forward, left, or right), the player must either direct Tux towards the flag, or pick the correct path following a given set of directions.

In a similar vein, young geeks interested in how computers work can learn the basics of programming with their very own graphical language. By laying out blocks with arrows on them, pupils can give instructions, so Tux can reach his dinner. Related activities include learning about binary numbers using LEDs, building electric circuits, and using logic gates to build digital circuits.

![4](https://dot.kde.org/sites/dot.kde.org/files/programming_tux.png)

*Learn programming logic and help Tux get to his dinner!*

GCompris 2.0 also includes all the activities children throughout the world have come to enjoy and love, activities that cover natural science, physics, art, history, and more.

## Help GCompris Become Better

GCompris is Free Software and thus belongs to all of humanity. Join us and help GCompris become even better!

It's easy: you can help with ideas, feedback, graphics, development and translations. In fact, translations are the best place to start. The translations for Galician, Slovak, Chinese Simplified, Irish Gaelic, and Czech, for example, just need a small push, and we will be able to include them into GCompris. Other languages are included, but are still missing some texts, such as Russian, Turkish and Breton. If you are familiar with any of these languages, you can begin contributing to GCompris right now!

[Find out how to get started here!](https://gcompris.net/wiki/How_to_translate)

_Автор:_ Редакторы KDE Dot News  
_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://dot.kde.org/2021/12/17/gcompris-releases-version-20  

<!-- 💡: location → расположение -->
