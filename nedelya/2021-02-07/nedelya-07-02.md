﻿
# На этой неделе в KDE: KWin получил прямую отрисовку для полноэкранных приложений, а Gwenview уделили много внимания!

> 31 января – 7 февраля, основное — прим. переводчика

Работа над Plasma 5.21 подходит к концу, были устранены почти все проблемы, обнаруженные на стадии бета-тестирования. Сейчас идёт работа над следующим крупным обновлением! Начало будет многообещающим...

## Нововведения

В Gwenview [стало возможным отключение функции «Bird’s eye view» в правом нижнем углу при увеличении изображения.](https://bugs.kde.org/show_bug.cgi?id=426105) (Madhav Kanbur, Gwenview 21.04)

В сессии Plasma, при использовании протокола Wayland, оконный менеджер KWin получил [прямую отрисовку для полноэкранных приложений](https://invent.kde.org/plasma/kwin/-/merge_requests/502) (напр., игры). В конечном итоге это повысит производительность и снизит задержки. (Xaver Hugl, Plasma 5.22)

## Исправления ошибок и улучшения производительности

Gwenview [больше не проявляет визуальные артефакты при панорамировании и увеличении изображения при использовании масштабного коэффициента высокого разрешения.](https://invent.kde.org/graphics/gwenview/-/merge_requests/35) (Влад Загородний, Gwenview 20.12.2)

Средство выбора качества JPEG в Gwenview [теперь снова работает.](https://invent.kde.org/graphics/gwenview/-/merge_requests/36) (Madhav Kanbur, Gwenview 20.12.3)

Gwenview [использует новую функцию отрисовки OpenGL, которая улучшает переходы между изображениями с помощью аппаратного ускорения, что позволяет работать этой функции в сессии Wayland, что позволило исправить различные ошибки и сбои.](https://invent.kde.org/graphics/gwenview/-/merge_requests/37) (Madhav Kanbur, Gwenview 20.12.3)

Исправлена [регрессия KRunner, когда приоритет в совпадении при поиске файлов отдавался неточным фразам из нескольких слов, вместо точного совпадения из одного слова.](https://bugs.kde.org/show_bug.cgi?id=431609). Также [поиск теперь имеет более точное соответствие в целом.](https://bugs.kde.org/show_bug.cgi?id=432339) (Harald Sitter, Plasma 5.21)

Исправлена [проблема с отрисовкой изображения на системах с несколькими графическими процессорами в пользовательском сеансе Wayland.](https://bugs.kde.org/show_bug.cgi?id=431968) (Xaver Hugl, Plasma 5.21)

Исправлена [проблема в KWin с обновлением содержимого Firefox, использующего бэкенд Wayland при запуске с определенными специфическими параметрами.](https://bugs.kde.org/show_bug.cgi?id=428499) (Влад Загородний, Plasma 5.21)

Решена [проблема с производительностью на слабых графических процессорах Intel](https://invent.kde.org/plasma/kwin/-/merge_requests/627), особенно [при прокрутке страниц в браузере Firefox.](https://invent.kde.org/plasma/kwin/-/merge_requests/635) (Влад Загородний, Plasma 5.21)

Пункты меню [диалоговых окон приложений GTK приведены к нормальному размеру, который соответствует другим приложениям Qt/KDE.](https://bugs.kde.org/show_bug.cgi?id=431819) (Jan Blackquill, Plasma 5.21)

Dolphin [теперь не заканчивает свою работу аварийно, если попытаться пропустить копирование или перемещение файлов, во время длительной операции с перемещением или копированием больших файлов.](https://bugs.kde.org/show_bug.cgi?id=431731) (Ahmad Samir, Frameworks 5.79)

Приложения KDE, которые были [закрыты в развёрнутом состоянии, при повторном запуске будут открываться в развёрнутом виде. Свёрнутые приложения при закрытии и повторном запуске изначально будут свёрнуты.](https://bugs.kde.org/show_bug.cgi?id=430521) (Nate Graham, Frameworks 5.79)

## Улучшения пользовательского интерфейса

Вы [теперь можете дважды или трижды щелкнуть уведомление, чтобы выделить текст, как и в других текстовых представлениях](https://bugs.kde.org/show_bug.cgi?id=431398), что может быть полезно для быстрого выбора и копирования текста одноразового кода, отправленного вам с веб-сайта, показанного в уведомлении через функцию KDE Connect, пересылающей текстовые сообщения на ваш компьютер. (Kai Uwe Broulik, Plasma 5.22)

Уведомления, связанные с файловыми операциями, теперь [показывают директорию назначения в качестве интерактивной ссылки, по которой можно сразу перейти, при желании.](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/626) (Kai Uwe Broulik, Plasma 5.22):

![](https://pointieststick.files.wordpress.com/2021/02/clickable-link-in-file-notification.png?w=776)

Анимации системного лотка теперь более пространственно согласованы. [Вид меняется в противоположном направлении от значка, на который вы нажали. ](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/586)На системном лотке, который размещён в вертикальном положении, будет использована анимация плавного перехода, поскольку анимация вертикального перехода выглядит довольно странно. (Jan Blackquill, Plasma 5.22)

Иконка приложения Telegram в системном лотке [теперь использует правильные цвета и соответствует цветной схеме Plasma.](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/81) (Rocket Aaron, Frameworks 5.79):

![](https://pointieststick.files.wordpress.com/2021/02/telegram-tray-dark.png?w=528)
![](https://pointieststick.files.wordpress.com/2021/02/telegram-tray-light.png?w=524)

Вкладка "Загрузить "что-то..."" [теперь имеют упрощенный интерфейс сортировки и фильтрации.](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/101) (Dan Leinir Turthra Jensen, Frameworks 5.79):

![](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210205_132723.jpeg?w=1024)

Рейтинг контента во вкладке "Загрузить "что-то"" [теперь отображает число, соответствующее звездам.](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/92) (Dan Leinir Turthra Jensen, Frameworks 5.79):

![](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210202_083316.jpeg?w=1024)

## И ещё кое-что...

Лидер цели KDE [«Согласованность»](https://community.kde.org/Goals/Consistency), Никколо Венеранди, выпустил полезное видео, демонстрирующее людям основы создания тем для приложений Plasma! Посмотрите, если Вам будет интересно:

<https://www.youtube.com/watch?v=XrNWYt_vciA>

## Как вы можете помочь

Прочитайте эту [статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку фонду](https://kde.org/community/donations/) [KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Виталий Рябичев](https://vk.com/jistake)  
_Источник:_ https://pointieststick.com/2021/02/05/this-week-in-kde-kwin-gains-direct-scan-out-and-gwenview-gets-a-lot-of-love/  

<!-- 💡: get new [thing] → «Загрузить [что-то]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: directory → каталог -->
