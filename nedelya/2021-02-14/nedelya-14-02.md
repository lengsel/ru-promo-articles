# На этой неделе в KDE: в преддверии Plasma 5.21

> 7–14 февраля, основное — прим. переводчика

До выхода Plasma 5.21 осталось всего несколько дней, так что будьте готовы! И зацените все эти крутые штуки. Поезд не остановить!

## Нововведения

Плагин проекта Kate [теперь предоставляет возможность переключать ветки git прямо в главном окне](https://invent.kde.org/utilities/kate/-/merge_requests/227) (Waqar Ahmed, Kate 21.04):

<https://i.imgur.com/C8DnC7l.mp4>

Виджет громкости звука Plasma [теперь позволяет изменять звуковой профиль устройства прямо в виджете, без необходимости переходить куда-либо](https://bugs.kde.org/show_bug.cgi?id=372562) (Kai Use Broulik, Plasma 5.22):

![](https://pointieststick.files.wordpress.com/2021/02/profiles-in-the-applet-1.jpeg?w=1024)

## Исправления ошибок

Dolphin [больше не вылетает при просмотре больших папок в виде дерева](https://bugs.kde.org/show_bug.cgi?id=422282) (Felix Ernst, Dolphin 21.04)

[Plasma](https://bugs.kde.org/show_bug.cgi?id=398440) и [целый сеанс](https://bugs.kde.org/show_bug.cgi?id=418683) больше не будут вылетать при перетаскивании файла через панель задач в сеансе Plasma Wayland (David Edmundson, Plasma 5.21)

Исправлена ​​ошибка, [из-за которой виджеты иногда невозможно было удалить с панели](https://bugs.kde.org/show_bug.cgi?id=432014) (Niccolò Venerandi, Plasma 5.21)

Кнопки уведомления «Новый экран подключён» [снова работают](https://invent.kde.org/plasma/kscreen/-/merge_requests/13) (David Redondo, Plasma 5.21)

Виджеты мониторинга диска [теперь отображают правильную информацию о текущей пропускной способности](https://bugs.kde.org/show_bug.cgi?id=432637) и [больше не показывают «Всего загружено» вместо «Скорость загрузки»](https://bugs.kde.org/show_bug.cgi?id=430676) (David Redondo, Plasma 5.21)

Установщик Fedora [теперь работает в сеансе Plasma Wayland](https://bugs.kde.org/show_bug.cgi?id=432625) (Влад Загородний, Plasma 5.21)

Discover [больше не предлагает ложные дополнительные источники в меню «Источники» при просмотре страницы сведений Flatpak-приложений](https://bugs.kde.org/show_bug.cgi?id=432654) (Aleix Pol Gonzalez, Plasma 5.21)

KWin [теперь обнаруживает только что подключённые гарнитуры виртуальной реальности](https://invent.kde.org/plasma/kwin/-/merge_requests/661) (Xaver Hugl, Plasma 5.21)

Уменьшение яркости экрана до минимального уровня, при котором подсветка отключается, [больше не вызывает мерцание подсветки на мгновение, прежде чем она снова выключится](https://invent.kde.org/plasma/powerdevil/-/merge_requests/38) (Kai Uwe Broulik, Plasma 5.21)

Окно Discover «Оставить отзыв» [больше не слишком узкое](https://bugs.kde.org/show_bug.cgi?id=432807) (Nate Graham, Plasma 5.21.1)

Несколько оставшихся страниц настроек, которые открываются в отдельных окнах, [теперь снова выглядят хорошо](https://bugs.kde.org/show_bug.cgi?id=431855) (Ismael Asensio и Nate Graham, Frameworks 5.80):

![](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210211_113502.png?w=962)

## Улучшения производительности

Discover [теперь запускается значительно быстрее](https://invent.kde.org/plasma/discover/-/merge_requests/66) (Aleix Pol Gonzalez, Plasma 5.21)

Немного улучшена [скорость запуска всех приложений, использующих Kirigami](https://invent.kde.org/frameworks/kirigami/-/merge_requests/212) (Arjen Hiemstra, Frameworks 5.80)

## Улучшения пользовательского интерфейса

Gwenview [теперь позволяет изменять качество/уровень сжатия для других форматов изображений с потерями, таких как WebP, AVIF, HEIF и HEIC](https://invent.kde.org/graphics/gwenview/-/merge_requests/33) (Nate Graham, Gwenview 21.04)

Kate [теперь по умолчанию включает функцию вырезания или копирования текущей строки, когда ничего не выбрано и вы используете действие «вырезать» или «копировать»](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/100) (Alexander Lohnau, Kate 21.04)

Изменение размера окна Dolphin [теперь меняет расположение значков с помощью единой плавной анимации, а не странной двухчастной анимации](https://invent.kde.org/system/dolphin/-/merge_requests/170) (Felix Ernst, Dolphin 21.04)

Нажатие Esc в полноэкранном режиме Okular [теперь возвращает к оконному представлению](https://bugs.kde.org/show_bug.cgi?id=366276) (Michael Augaitis, Okular 21.04)

В новом виджете раскладки клавиатуры для Wayland [текст теперь масштабируется по толщине панели, на которой он расположен](https://bugs.kde.org/show_bug.cgi?id=431889) (Андрей Бутырский, Plasma 5.21)

[Теперь вы можете настроить виджет «Диски и устройства» для воспроизведения звукового уведомления, когда устройство можно безопасно удалить](https://bugs.kde.org/show_bug.cgi?id=384469) (Nate Graham, Plasma 5.22)

## Сетевое присутствие (о, давайте попробуем новый раздел)

Carl Schwan [переделал сайт Kate, чтобы там использовалась наша новая модная тема](https://kate-editor.org/):

![](https://pointieststick.files.wordpress.com/2021/02/kates-new-website.png?w=841)

Niccolò Venerandi опубликовал вторую часть своей серии видео о том, как создать тему для Plasma:
<https://www.youtube.com/watch?v=3K4EP1KSC6k>

Leszek Lesner опубликовал видео об инструменте [kio-fuse](https://vk.com/kde_ru?w=wall-33239_6287), который упрощает взаимодействие с удалёнными файлами:
<https://www.youtube.com/watch?v=KqpPlFo7OjY>

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку фонду](https://kde.org/community/donations/) [KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Максим Маршев (PeWpIC)](https://t.me/PeWpIC)  
_Источник:_ https://pointieststick.com/2021/02/12/this-week-in-kde-plasma-5-21-approaches/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить [что-то]» -->
<!-- 💡: icons → значки -->
<!-- 💡: locations → расположения -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
