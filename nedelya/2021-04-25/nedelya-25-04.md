# На этой неделе в KDE: улучшения интерфейса и спец. возможностей

На этой неделе у нас обычный ассортимент исправлений ошибок и несколько небольших новых функций, которые вы ждали. Но основное внимание было уделено улучшениям пользовательского интерфейса и специальных возможностей — их действительно очень много!

> 18–25 апреля, основное — прим. переводчика

## Новые возможности

В Dolphin появилась [возможность](https://invent.kde.org/system/dolphin/-/merge_requests/193) быстро начать переименование следующего или предыдущего файла с помощью комбинации клавиш Tab/Shift+Tab или же стрелками вверх/вниз в панели «Сведения» (Michal Sciubidlo и Méven Car, Dolphin 21.08)

Страница «Поиск файлов» в Параметрах системы теперь [позволяет](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/427) проверять статус индексации, временно приостанавливать или возобновлять её, а также отслеживать текущие действия индексатора. Кроме того, при отключении индексирования [предлагается](https://bugs.kde.org/show_bug.cgi?id=414077) удалить базу данных индекса с диска (Nate Graham и David Edmundson, Plasma 5.22):

![0](https://pointieststick.files.wordpress.com/2021/04/indexing-1.png?w=2418)

![1](https://pointieststick.files.wordpress.com/2021/04/delete-it-1.png?w=2364)

Дополнение Plasma Browser Integration теперь [предупреждает](https://invent.kde.org/plasma/plasma-browser-integration/-/merge_requests/44), когда загрузка файла ещё не началась, потому что браузер ждёт нажатия кнопки «Да, я принимаю риск загрузки файлов и бла-бла-бла» (Kai Uwe Broulik, Plasma 5.22):

![2](https://pointieststick.files.wordpress.com/2021/04/screenshot_20210409_215130.png?w=794)

## Исправления и улучшения производительности

В сеансе Plasma Wayland больше [не возникает](https://bugs.kde.org/show_bug.cgi?id=398440) сбой при перетаскивании файла над панелью (Alois Wohlschlager, Plasma 5.21.5)

Помните ту досадную ошибку, из-за которой процент громкости иногда менялся слишком сильно или недостаточно и которую мы считали исправленной? Оказалось, что нет, но теперь точно [исправили](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/57), честно! (Chris Holland — так что можно быть уверенным в правильности, Plasma 5.21.5)

Помните также другую неприятную повторяющуюся ошибку, из-за которой всплывающее меню настройки виджета в режиме редактирования панели, когда панель расположена сверху или слева, иногда исчезало при наведении на него указателя мыши? Это тоже наконец-то [позади](https://bugs.kde.org/show_bug.cgi?id=413736)! (Niccolò Venerandi, Plasma 5.21.5)

Анимации переключения окон «Карусель» и «Перелистывание» теперь [снова работают](https://bugs.kde.org/show_bug.cgi?id=433471) правильно (Влад Загородний, Plasma 5.21.5)

В приложениях GTK теперь [используется правильный значок](https://invent.kde.org/plasma/breeze-gtk/-/merge_requests/20) стрелки выпадающего списка (Jan Blackquill, Plasma 5.21.5)

![custom-1](Screenshot_20210420_182805.png)

[Исправлена ошибка](https://bugs.kde.org/show_bug.cgi?id=435143), из-за которой индексатор файлов мог аварийно завершить работу при попытке индексировать перемещённый или переименованный каталог (Oded Arbel, Frameworks 5.82)

Щелчок по затемнённой области за всплывающим диалогом в приложениях, использующих Kirigami, снова [закрывает этот диалог](https://bugs.kde.org/show_bug.cgi?id=435523) (David Edmundson, Frameworks 5.82)

## Специальные возможности

Страница «Комбинации клавиш» Параметров системы теперь [поддерживает](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/431) функцию чтения с экрана и управление без мыши (Carl Schwan, Plasma 5.22)

Значки часто используемых разделов на стартовой странице Параметров системы тоже теперь [поддерживают](https://invent.kde.org/plasma/plasma-desktop/commit/43673d3d0cd6ccb9d933b1a856ee61525037d56c) функцию чтения с экрана и выбор клавиатурой (Nate Graham, Plasma 5.22)

Различные элементы, расположенные сеткой в разделах Параметров системы, также стали полностью [совместимы](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/57) с экранным диктором и управлением без мыши (Jan Blackquill, Frameworks 5.82)

## Улучшения пользовательского интерфейса

Okular теперь [позволяет отключить](https://invent.kde.org/graphics/okular/-/merge_requests/407) отображение больших уведомлений о встроенных файлах, формах или подписях (Okular 21.08)

[Виджет «Календарь»](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/224) и [всплывающее окно](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/727) виджета «Цифровые часы», которое содержит его, были полностью переделаны для более современного и последовательного вида (Carl Schwan, Plasma 5.22 и Frameworks 5.82):

![4](https://pointieststick.files.wordpress.com/2021/04/screenshot_20210423_113135.png?w=1024)

Скоро мы заменим эти маленькие острые треугольники точками.

[Переработаны](https://bugs.kde.org/show_bug.cgi?id=432703) легенды графиков Системного монитора, что особенно улучшает представление графиков нагрузки ЦП при большом числе ядер (Arjen Hiemstra, Plasma 5.22):

![6](https://pointieststick.files.wordpress.com/2021/04/screenshot_20210421_140457.png?w=1024)

Страницы [«Рабочие столы»](https://invent.kde.org/plasma/kwin/-/merge_requests/788) и [«Эффекты»](https://invent.kde.org/plasma/kwin/-/merge_requests/737) в Параметрах системы теперь поддерживают функцию «Выделить изменённые параметры» (Benjamin Port, Plasma 5.22)

Панели Plasma [больше не исчезают](https://bugs.kde.org/show_bug.cgi?id=178913), когда активен эффект «Все окна», а значки приложений при этом [стали крупнее и отображаются в центре окон](https://invent.kde.org/plasma/kwin/-/merge_requests/868) (Felipe Kinoshita, Plasma 5.22):

![7](https://pointieststick.files.wordpress.com/2021/04/screenshot_20210421_100615-1.png?w=1024)

Эффект «Показать рабочий стол» теперь просто [скрывает все окна](https://invent.kde.org/plasma/kwin/-/merge_requests/691), не оставляя их небольшие полупрозрачные части в углах экрана (Nate Graham, Plasma 5.22):

[https://i.imgur.com/Us2NKe0.mp4](https://i.imgur.com/Us2NKe0.mp4)

Новый Системный монитор Plasma теперь по умолчанию [сортирует](https://bugs.kde.org/show_bug.cgi?id=429149) приложения по использованию памяти, а не по имени (Felipe Kinoshita, Plasma 5.22)

Виджет «Батарея и яркость» теперь [открывает](https://bugs.kde.org/show_bug.cgi?id=423761) страницу энергосбережения в Параметрах системы, а не в забавном маленьком отдельном окне (Евгений Попов, Plasma 5.22)

Значки на панели Plasma теперь [меняют свой размер](https://bugs.kde.org/show_bug.cgi?id=434324) более плавно при изменении её высоты (Niccolò Venerandi, Frameworks 5.82)

## Ещё кое-что...

Строго говоря, это не о проекте KDE, но, тем не менее, для нас актуально: диспетчер входа в систему SDDM теперь может [запускать экран входа](https://github.com/sddm/sddm/issues/246) без прав суперпользователя! Работа была выполнена Pier Luigi Fiorini и войдёт в SDDM 0.20.

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)
_Перевод:_ [Илья Андреев](https://vk.com/ilyandl)
_Источник:_ https://pointieststick.com/2021/04/23/this-week-in-kde-overflowing-with-ui-and-accessibility-improvements/
