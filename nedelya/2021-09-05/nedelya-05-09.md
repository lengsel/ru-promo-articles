# На этой неделе в KDE: море исправлений

…многие из которых — для сеанса Plasma Wayland! Он наконец-то приближается к стабильности. Я сейчас использую Plasma Wayland на повседневной основе. На данный момент все мои основные придирки касаются сторонних приложений, а не программного обеспечения KDE. Да, мы долго к этому шли, но я думаю, что мы почти у цели!

> 29 августа – 5 сентября, основное — прим. переводчика

В любом случае вот весь список:

## Новые возможности

Когда вы нажимаете кнопку «Применить» на странице «Настройка экранов» Параметров системы, она теперь [предлагает отменить любые внесённые изменения](https://invent.kde.org/plasma/kscreen/-/merge_requests/35?commit_id=ded3aa249aa3b56a9d0fff9435c3ebf4487c003c), способные привести к сломанной конфигурации. Отмена происходит автоматически по истечении 30 секунд — на случай, если с новыми настройками экранов кнопка отмены и вовсе не видна (Chris Rizzitello и Zixing Liu, Plasma 5.23):

![0](https://pointieststick.files.wordpress.com/2021/08/screenshot_20210829_111622.png)

## Wayland

В сеансе Plasma Wayland [теперь можно настроить опцию «Broadcast RGB»](https://bugs.kde.org/show_bug.cgi?id=375666) драйвера видеокарт Intel (Xaver Hugl, Plasma 5.23)

В сеансе Wayland теперь [можно перетаскивать данные между нативными приложениями Wayland и XWayland](https://bugs.kde.org/show_bug.cgi?id=437406)! (David Redondo, Plasma 5.23)

В сеансе Wayland теперь [можно изменять разрешение экрана при запуске на виртуальной машине](https://bugs.kde.org/show_bug.cgi?id=407058) (Méven Car, Plasma 5.23)

В сеансе Wayland виртуальные рабочие столы теперь [запоминаются для каждой комнаты](https://bugs.kde.org/show_bug.cgi?id=439183) (David Redondo, Plasma 5.23)

Для пользователей Plasma Wayland с видеокартами NVIDIA исправлены [отрисовка содержимого окон при изменении их размера](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/71) и [отображение списка результатов поиска KRunner](https://bugs.kde.org/show_bug.cgi?id=432062) (David Redondo, Frameworks 5.86)

В сеансе Wayland изображения, скопированные из Spectacle, теперь [отображаются правильно](https://bugs.kde.org/show_bug.cgi?id=409798) (Jan Blackquill, Qt 6.2 или Qt 5.15.3 с набором патчей KDE)

В сеансе Wayland перетаскивание файла над окном больше [не приводит к немедленному перемещению этого окна на передний план](https://bugs.kde.org/show_bug.cgi?id=440534); добавлена задержка, как в сеансе X11 (Xaver Hugl, Plasma 5.22.5)

Эффект размытия фона [больше не такой зернистый при использовании Wayland](https://invent.kde.org/plasma/kwin/-/merge_requests/1281) (Tatsuyuki Ishi, Plasma 5.23)

## Исправления ошибок и улучшения производительности

При переименовании файла или папки, соответствующих текущему тексту фильтра в Dolphin, они теперь [исчезают из поля зрения, когда новое имя больше не соответствует тексту фильтра](https://bugs.kde.org/show_bug.cgi?id=439517) (Евгений Попов, Dolphin 21.08.1)

Действие «Открыть в терминале» в Dolphin [больше не завершается неудачей в некоторых случаях](https://bugs.kde.org/show_bug.cgi?id=441072) (Nate Graham, Dolphin 21.12, но я рекомендовал дистрибутивам включить исправление в 21.08)

Внешние носители [снова отображаются, как и ожидалось, в виджете «Диски и устройства» после отключения и повторного подключения](https://bugs.kde.org/show_bug.cgi?id=438874) (Fabio Bas, Plasma 5.23)

Приложения Flatpak, отправляющие уведомления, теперь [правильно идентифицируются](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/968) (Kai Uwe Broulik, Plasma 5.23)

Системный монитор и его виджеты теперь [обнаруживают больше показаний датчиков видеокарт AMD](https://bugs.kde.org/show_bug.cgi?id=438318) (David Redondo, Plasma 5.23)

Раскрывающиеся элементы списков в виджетах Plasma теперь [могут полноценно управляться стилусом](https://bugs.kde.org/show_bug.cgi?id=426079) и [больше не отображают иногда содержимое причудливо перекрывающимся, когда виджет прокручивается](https://bugs.kde.org/show_bug.cgi?id=428102) (Nate Graham, Frameworks 5.86)

Приложения, использующие Kirigami, стали [запускаться](https://invent.kde.org/frameworks/kirigami/-/merge_requests/355) [значительно быстрее](https://invent.kde.org/frameworks/kirigami/-/merge_requests/356) (Arjen Hiemstra, Frameworks 5.86)

## Улучшения пользовательского интерфейса

Двойной щелчок по разделителю в двухпанельном режиме Dolphin теперь [выравнивает его по центру](https://bugs.kde.org/show_bug.cgi?id=206525) (Евгений Попов, Dolphin 21.12)

При использовании офлайн-обновлений (т.е. установке обновлений в ходе перезагрузки) Discover [больше не будет раздражать вас настойчивыми просьбами выполнить перезагрузку](https://invent.kde.org/plasma/discover/-/merge_requests/156), ведь с ней незачем торопиться (Nate Graham, Plasma 5.23)

Страница «Звуковые устройства» Параметров системы теперь [объединяет все функций подраздела настройки с соответствующими элементами основного представления, на которые они влияют](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/71), что упрощает доступ к ним и избавляет от лишней вложенности (Ismael Asensio, Plasma 5.23):

![1](https://pointieststick.files.wordpress.com/2021/09/all-on-one-page.png)

Всплывающие окна системного лотка с раскрывающимися элементами списков были [значительно улучшены в плане их внешней согласованности, скорости отклика при прокрутке, управляемости клавиатурой и общей стабильности](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/330) (Nate Graham, Frameworks 5.86)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://pointieststick.com/2021/09/03/this-week-in-kde-gazillions-of-bugfixes/  
