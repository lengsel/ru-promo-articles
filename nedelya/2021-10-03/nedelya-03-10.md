# На этой неделе в KDE: готовим Plasma 5.23 к выпуску

> 26 сентября – 3 октября, основное — прим. переводчика

Мы продолжаем отлавливать дефекты в бета-версии Plasma 5.23, чтобы к итоговому выпуску она была в отличной форме! Как и на прошлой неделе, я настойчиво прошу всех, кто обладает соответствующими навыками, помочь с исправлением ошибок из [этого списка](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=REOPENED&keywords=regression&keywords_type=allwords&list_id=1916669&order=priority%2Cchangeddate%20DESC%2Cdupecount%20DESC%2Cbug_status%2Cassigned_to%2Cbug_id&query_format=advanced&version=5.22.90). Любой вклад приветствуется.

## Новые возможности

Konsole [теперь позволяет менять цветовую схему самого приложения (не области терминала, а интерфейса вокруг) независимо от глобальной цветовой схемы](https://invent.kde.org/utilities/konsole/-/merge_requests/493) (Maximillien di Dio и Ahmad Samir, Konsole 21.12):

![0](https://pointieststick.files.wordpress.com/2021/09/screenshot_20210927_124826.png)

## Wayland

В сеансе Plasma Wayland [заработало быстрое переключение пользователей](https://bugs.kde.org/show_bug.cgi?id=439873) (Влад Загородний и Xaver Hugl, Plasma 5.23)

В сеансе Wayland [диспетчер окон KWin больше не завершается аварийно, когда некоторые приложения отображают контекстные меню и другие всплывающие окна](https://bugs.kde.org/show_bug.cgi?id=438097) (Влад Загородний, Plasma 5.23)

В сеансе Wayland [KWin больше не прекращает работу при выходе системы из спящего режима](https://bugs.kde.org/show_bug.cgi?id=442603) (Влад Загородний, Plasma 5.23)

В сеансе Wayland [конфигурации из двух мониторов, отображающих один и тот же видеовыход, стали правильно определяться на странице «Настройка экранов» Параметров системы](https://bugs.kde.org/show_bug.cgi?id=435265) (Xaver Hugl, Plasma 5.23)

В сеансе Wayland [выявление бездействия пользователя для автоматической блокировки экрана стало работать надёжнее](https://invent.kde.org/plasma/kwin/-/merge_requests/1436) (Méven Car, Plasma 5.24)

## Исправления ошибок и улучшения производительности

Центр приложений Discover [снова может использоваться для удаления приложений](https://bugs.kde.org/show_bug.cgi?id=442383), после того, как эта возможность сломалась из-за [неожиданного изменения в библиотеке PackageKit](https://github.com/PackageKit/PackageKit/commit/86f4a2fbf5fcb1230c68a57c4a7bc066aa3f7ef4) (Antonio Rojas, Plasma 5.23)

Раскладки клавиатуры, не входящие в кольцо переключения, [снова можно выбирать через контекстное меню виджета-переключателя](https://bugs.kde.org/show_bug.cgi?id=441239) (Андрей Бутырский, Plasma 5.23)

Поиск в Discover [стал куда надёжнее, особенно при использовании сразу после запуска. Заодно проверка доступных обновлений сильно ускорилась!](https://bugs.kde.org/show_bug.cgi?id=432965) (Aleix Pol Gonzalez, Plasma 5.24)

Discover [теперь быстрее загружает первичные данные в любой из категорий дополнений](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/144) (Aleix Pol Gonzalez, Frameworks 5.87)

## Улучшения пользовательского интерфейса

При включённом восстановлении сеанса [Spectacle больше не запускается при входе в сеанс, если был открыт при последнем выходе](https://bugs.kde.org/show_bug.cgi?id=430411) (Иван Ткаченко, Spectacle 21.12)

Клавиши Home и End [теперь перемещают выбор соответственно к первому и последнему результату поиска KRunner, когда поисковая строка не активна](https://bugs.kde.org/show_bug.cgi?id=436897) (Alexander Lohnau, Plasma 5.24)

Раздел «Клавиатура» Параметров системы [теперь умеет выделять изменённые параметры](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/519) (Cyril Rossi, Plasma 5.24)

[В теме Breeze появились идеальные по пикселям версии значков настроек в размере 22x22 пикс.](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/124), что должно улучшить внешний вид этих значков везде, где они отображаются в таком размере, в том числе в боковой панели Параметров системы (Manuel Jesús de la Fuente, Frameworks 5.87)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Бизяев](https://vk.com/ilya_bizyaev)  
_Источник:_ https://pointieststick.com/2021/10/01/this-week-in-kde-getting-plasma-5-23-ready-for-release/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: thumbnail → миниатюра -->
