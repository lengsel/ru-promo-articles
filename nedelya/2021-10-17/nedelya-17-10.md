# На этой неделе в KDE: После выпуска Plasma 5.23

> 10–17 октября, основное — прим. переводчика

Только состоялся юбилейный выпуск Plasma 5.23, приуроченный к 25-летию KDE, но готов поспорить, что после прочтения этой новости вы захотите уже Plasma 5.24! Много работы было проделано на этой неделе по части навигации с клавиатуры и вокруг Discover, а также исправлено множество ошибок.

## Новые возможности

Skanlite [теперь поддерживает сканирование в PDF](https://bugs.kde.org/show_bug.cgi?id=299517)! (пока что по одной странице за раз; впрочем, Skanpage поддерживает многостраничные PDF-сканы) (Alexander Stippich, Skanlite 21.12)

Gwenview [теперь показывает предварительную оценку нового размера файла в процессе изменения размеров изображения](https://bugs.kde.org/show_bug.cgi?id=433740) (Antonio Prcela, Gwenview 21.12):

![0](https://pointieststick.files.wordpress.com/2021/10/screenshot_20211013_133602.png)

## Wayland

Сеанс Plasma Wayland больше [не падает сразу после входа, если вы используете опцию «Правая Alt никогда не выбирает 3-й ряд» в дополнительных настройках клавиатуры](https://bugs.kde.org/show_bug.cgi?id=440027) (Андрей Бутырский, Plasma 5.23.1)

В сеансе Wayland [окна некоторых приложений больше не открываются в минимально возможном размере при первом запуске этих приложений](https://bugs.kde.org/show_bug.cgi?id=443705) (Влад Загородний, Plasma 5.23.1)

В сеансе Wayland распахнутые приложения GNOME [теперь полностью обновляют содержимое всего окна, а не только бóльшую его часть](https://bugs.kde.org/show_bug.cgi?id=443756) (Влад Загородний, Plasma 5.23.1)

Сеанс Wayland [больше не падает время от времени в определённых условиях, когда вы многократно наводите и уводите курсор с миниатюр панели задач](https://bugs.kde.org/show_bug.cgi?id=439681) (Влад Загородний, Frameworks 5.88)

В сеансе Wayland [виртуальная клавиатура теперь появляется, только когда вы явно передаёте фокус текстовому элементу нажатием на сенсорный экран либо стилусом](https://invent.kde.org/plasma/kwin/-/merge_requests/1517) (Aleix Pol Gonzalez, Plasma 5.24)

Автоматический поворот экрана [теперь работает при включённой опции «Только в режиме планшета»](https://bugs.kde.org/show_bug.cgi?id=428626) (John Clark, Plasma 5.23)

## Исправления ошибок и улучшения производительности

Меню закладок Okular [теперь должным образом перезагружается и продолжает показывать корректный набор закладок, когда вы переключаетесь между документами](https://bugs.kde.org/show_bug.cgi?id=442668) (Albert Astals Cid, Okular 21.08.3)

Spectacle [теперь правильно передаёт цвета в снимках с экранов со включённой поддержкой глубины цвета 10 бит на канал](https://bugs.kde.org/show_bug.cgi?id=422285) (Bernie Innocenti, Spectacle 21.12)

Ручной ввод имени пользователя и пароля на экране входа в систему [снова работает](https://bugs.kde.org/show_bug.cgi?id=443737) (Nate Graham, Plasma 5.23.1, а дистрибутивам стоит немедленно применить этот патч)

Закрытие окна Firefox [больше не может вызывать аварийное завершение диспетчера окон KWin](https://bugs.kde.org/show_bug.cgi?id=443765) (Влад Загородний, Plasma 5.23.1)

Фоновый сервис `kded5` [теперь не будет аварийно завершаться при подключении нескольких мониторов](https://bugs.kde.org/show_bug.cgi?id=442822) (Fabian Vogt, Plasma 5.23.1)

Discover [больше не падает на странице «Установленные» в дистрибутивах наподобие Gentoo, которые не предоставляют собранных пакетов](https://bugs.kde.org/show_bug.cgi?id=443745), и вы используете Discover для установки из Flatpak или Snap. (Aleix Pol Gonzalez, Plasma 5.23.1)

Правый щелчок мыши по файлу на рабочем столе, когда их выделено несколько, [больше не убирает выделение с остальных файлов](https://bugs.kde.org/show_bug.cgi?id=443743) (Nate Graham, Plasma 5.23.1)

OpenConnect VPN [теперь подключается как полагается, если у вас пароль на основе FSID, с пользовательским сертификатом, но без приватного ключа](https://bugs.kde.org/show_bug.cgi?id=443770) (Raphael Kubo da Costa, Plasma 5.23.1)

Переключение между областями просмотра виджета «Доска приложений» [стало быстрее](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/598) (David Edmundson, Plasma 5.23.1)

Меню [больше не имеют избыточной обводки по углам при использовании дробных значений глобального масштаба](https://bugs.kde.org/show_bug.cgi?id=418166) (Tatsuyuki Ishi, Plasma 5.24)

Полоса прокрутки на боковой панели «Виджеты» [теперь прячется, когда всё содержимое умещается на экране](https://bugs.kde.org/show_bug.cgi?id=426077) (Méven Car, Plasma 5.24)

Ползунки звука в виджете «Громкость» [снова имеют фон](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/88); два разных цвета используются, чтобы отличать максимальную громкость от текущей записываемой или воспроизводимой (Tanbir Jishan, Plasma 5.24):

![2](https://pointieststick.files.wordpress.com/2021/10/screenshot_20211013_125330.png)

Отправка файла через меню «Поделиться» в Telegram, установленный из Flatpak, [снова работает](https://invent.kde.org/frameworks/purpose/-/merge_requests/40) (Александр Керножицкий, Frameworks 5.88)

Вернули возможность [менять значок виджета, запускающего приложения с панели](https://bugs.kde.org/show_bug.cgi?id=441847) (Fabio Bas, Frameworks 5.88)

Плеяды проблем, связанных как с [остаточными изображениями окон после переключения рабочих столов](https://bugs.kde.org/show_bug.cgi?id=438552), так и с их [полным исчезновением после использования виджета «Показать рабочий стол»](https://bugs.kde.org/show_bug.cgi?id=440751), были исправлены (Влад Загородний, Qt 5.15.3 — набор патчей KDE)

## Улучшения пользовательского интерфейса

Виджет «Сетевые подключения» [стал полностью управляемым с клавиатуры](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/81), включая такие тонкости, как стрелка вниз, ведущая из поиска к первому элементу списка, и [клавиша Tab, перемещающая фокус между кнопками в подсвеченной строчке](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/351) (Bharadwaj Raju, Plasma 5.24)

В том же духе, виджет «Буфер обмена» [также стал полностью управляемым с клавиатуры](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1094)! (Bharadwaj Raju, Plasma 5.24)

Discover теперь пытается помочь не самым технически подкованным пользователям, [объясняя ситуацию и предлагая поискать в интернете приложение, если оно не было найдено](https://invent.kde.org/plasma/discover/-/merge_requests/185) (Nate Graham, Plasma 5.24):

![3](https://pointieststick.files.wordpress.com/2021/10/nothing-found-in-a-search.png)

Discover теперь [отображает нижнюю панель вкладок в узком (мобильном) режиме](https://invent.kde.org/plasma/discover/-/merge_requests/173), а [кнопка открытия шторки больше не перекрывает содержимое](https://invent.kde.org/frameworks/kirigami/-/merge_requests/391) (Aleix Pol Gonzalez, Plasma 5.24):

![4](https://pointieststick.files.wordpress.com/2021/10/screenshot_20211011_094519.png)

Discover [теперь показывает содержимое на домашнем экране в две колонки, когда окно достаточно широкое](https://invent.kde.org/plasma/discover/-/merge_requests/180) (Felipe Kinoshita, Plasma 5.24):

![5](https://pointieststick.files.wordpress.com/2021/10/screenshot_20211011_210606.png)

Уведомления о видеофайлах [теперь отображают миниатюры в самих уведомлениях](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1109), так же, как и для изображений (Kai Uwe Broulik, Plasma 5.24):

![6](https://pointieststick.files.wordpress.com/2021/10/screenshot_20211012_141200.png)

Область и текст заголовка уведомлений [теперь имеют лучшую видимость и контрастность](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1110?) (Nate Graham, Plasma 5.24):

![7](https://pointieststick.files.wordpress.com/2021/10/screenshot_20211015_152039.png)

Диалог «Добавить раскладку» [теперь гораздо проще и удобнее](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/596) (Bharadwaj Raju, Plasma 5.24):

![8](https://pointieststick.files.wordpress.com/2021/10/screenshot_20211013_105813.png)

Заголовки разделов в формах Kirigami [теперь горизонтально отцентрированы и немного крупнее](https://invent.kde.org/frameworks/kirigami/-/merge_requests/357) (Nate Graham, Frameworks 5.88):

![10](https://pointieststick.files.wordpress.com/2021/10/screenshot_20211015_080522.png)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Иван Ткаченко (ratijas)](https://t.me/ratijas)  
_Источник:_ https://pointieststick.com/2021/10/15/this-week-in-kde-plasma-25th-anniversary-edition-is-released/  

<!-- 💡: applet → виджет -->
<!-- 💡: daemon → служба -->
<!-- 💡: icon → значок -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
