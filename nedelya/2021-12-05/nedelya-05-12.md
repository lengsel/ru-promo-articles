# На этой неделе в KDE: новые функции в Spectacle и тонны исправлений

> 28 ноября – 5 декабря, основное — прим. переводчика

## Новые возможности

Spectacle [теперь позволяет комментировать существующие снимки экрана через кнопку в уведомлении или параметром командной строки `--edit-existing <file>`](https://bugs.kde.org/show_bug.cgi?id=431257) (Bharadwaj Raju, Spectacle 22.04):

![0](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211203_222201.png)

## Wayland

В сеансе Plasma Wayland коэффициент масштабирования, отображаемый в Параметрах системы на странице «Экран», [больше не округляется в меньшую сторону при использовании дробного масштабирования, например, 150%](https://invent.kde.org/plasma/kscreen/-/merge_requests/60) (Méven Car, Plasma 5.24)

В сеансе Wayland имена мониторов [больше не дублируются странным образом на странице «Экран» Параметров системы](https://invent.kde.org/plasma/kscreen/-/merge_requests/55) (Méven Car, Plasma 5.24)

В сеансе Wayland [теперь работает анимация преобразования всплывающих окон, так что подсказки панели будут плавно анимироваться по мере их появления и исчезновения, как в сеансе X11](https://bugs.kde.org/show_bug.cgi?id=446375) (Marco Martin, Frameworks 5.89)

## Исправления ошибок

Открытие списков воспроизведения формата `.m3u*` в проигрывателе Elisa из диспетчера файлов [стало работать правильно](https://bugs.kde.org/show_bug.cgi?id=441544) (Bharadwaj Raju, Elisa 22.04)

Статус Bluetooth [теперь сохраняется при выходе из системы, если используется опция «Запоминать предыдущее состояние»](https://bugs.kde.org/show_bug.cgi?id=445376) (Nate Graham, Plasma 5.23.5)

Discover [больше не падает, когда вы пытаетесь открыть страницу с описанием приложения Flatpak, которое вы только что удалили](https://bugs.kde.org/show_bug.cgi?id=445819) (Aleix Pol Gonzalez, Plasma 5.24)

Регулировка яркости экрана [теперь всегда работает правильно](https://bugs.kde.org/show_bug.cgi?id=399646) на устройствах с несколькими видеокартами (Dan Robinson, Plasma 5.24)

Виджет «Проигрыватель» [теперь правильно показывает «Ничего не воспроизводится», когда закрывается последнее приложение, воспроизводящее мультимедиа](https://bugs.kde.org/show_bug.cgi?id=446098) (Fushan Wen, Plasma 5.24)

[Миниатюры панели задач теперь правильно отображают элементы управления мультимедиа](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/725), если произошёл перезапуск приложения или вкладки браузера, воспроизводящих мультимедиа (Bharadwaj Raju, Plasma 5.24)

Ввод поискового запроса в панели выбора эмодзи сразу после её вызова [теперь работает правильно](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/730) (Bharadwaj Raju, Plasma 5.24)

Элементы оформления рабочего стола Plasma [больше не отображаются с причудливыми искажениями после того, как они были изменены в новой версии](https://bugs.kde.org/show_bug.cgi?id=445516) (Marco Martin, Frameworks 5.89)

Монохромные значки Breeze [снова отображаются в правильном цвете при использовании тёмной цветовой схемы](https://bugs.kde.org/show_bug.cgi?id=445489) (Rodney Dawes, Frameworks 5.89)

При отсутствии запрошенного значка в текущем наборе [он снова сначала будет заменён на следующий ближайший значок из этого набора (например, `edit-copy-location` — на `edit-copy`), а не на соответствующий значок из резервного набора](https://bugs.kde.org/show_bug.cgi?id=445804) (Janet Blackquill, Frameworks 5.89)

## Улучшения производительности

Панели Plasma [теперь загружаются быстрее при входе в систему и в это время выглядят менее глючно](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/715) (David Edmundson, Plasma 5.23.5)

Discover [стал быстрее проверять наличие обновлений для приложений Flatpak](https://invent.kde.org/plasma/discover/-/merge_requests/213) (Aleix Pol Gonzalez, Plasma 5.24)

Приложение и виджеты Системного монитора теперь используют меньше ресурсов, поскольку не опрашивают постоянно [диски](https://invent.kde.org/plasma/ksystemstats/-/merge_requests/26) и [датчики](https://invent.kde.org/plasma/libksysguard/-/merge_requests/197) о данных, которые не отображаются (Arjen Hiemstra, Plasma 5.24)

## Улучшения пользовательского интерфейса

Временные задания, которые отображают уведомления с текстом вроде «Проверка…» или «Открытие…», [больше не будут отображать уведомление «Завершено…» при успешном завершении](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1206) (Kai Uwe Broulik, Plasma 5.24)

Когда кнопка «Закладки» добавляется на панель инструментов Konsole, [её всплывающее окно теперь можно открыть обычным щелчком, а не щелчком и удержанием](https://bugs.kde.org/show_bug.cgi?id=446339) (Nate Graham, Konsole 21.12)

Spectacle [теперь учитывает последние использованные значения параметров «Включая курсор мыши» и «Включая заголовок и границы окна» при создании снимков экрана с использованием глобальной комбинации клавиш](https://bugs.kde.org/show_bug.cgi?id=392057) (Antonio Prcela, Spectacle 22.04)

Gwenview [теперь поддерживает большие миниатюры размером 512×512 и 1024×1024](https://invent.kde.org/graphics/gwenview/-/merge_requests/119) (Илья Поминов, Gwenview 22.04)

В выпадающем списке адресной строки Dolphin [теперь показываются скрытые файлы, если они видны в основной области](https://bugs.kde.org/show_bug.cgi?id=441617) (Евгений Попов, Dolphin 22.04)

Discover [теперь показывает вам разумное сообщение, когда у вас настроен модуль поддержки Flatpak без каких-либо репозиториев, и даже предлагает вам нажатием кнопки добавить Flathub](https://bugs.kde.org/show_bug.cgi?id=444239) (Aleix Pol Gonzalez, Plasma 5.24)

Когда вы используете систему на языке, отличном от английского, [поисковые запросы, введённые в поле поиска Параметров системы на английском, по-прежнему будут давать результаты](https://bugs.kde.org/show_bug.cgi?id=446285) (Fushan Wen, Plasma 5.24)

При использовании глобального масштабирования, в области визуализации на странице настройки экранов Параметров системы [теперь отображается физическое разрешение экранов, а не используемое логическое](https://bugs.kde.org/show_bug.cgi?id=441417) (Méven Car, Plasma 5.24):

![1](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211203_221425.png)

При наведении курсора на файл или папку в корзине [больше не происходит тайного копирования этого элемента в `/tmp` для создания миниатюры](https://bugs.kde.org/show_bug.cgi?id=368104) (Eduardo Sánchez Muñoz, Frameworks 5.89)

Полосы прокрутки, индикаторы выполнения и ползунки в оформлении рабочего стола Breeze [теперь имеют тот же немного более тёмный цвет фона, что и в окнах приложений](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/404) (S. Christian Collins, Frameworks 5.89):

![2](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211203_223119.png)

Всплывающие подсказки для элементов с сокращённым текстом в представлениях поиска KRunner [стали использовать тот же стиль, что и в других местах](https://invent.kde.org/plasma/milou/-/merge_requests/33) (David Redondo, Frameworks 5.89)

Поле поиска в диалоговом окне выбора значков [теперь можно выбрать с помощью сочетания клавиш Ctrl+F](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/50) (Kai Uwe Broulik, Frameworks 5.89)

Клавиша Esc [теперь закрывает диалоговые окна в приложениях на основе Kirigami](https://invent.kde.org/frameworks/kirigami/-/merge_requests/423) (Claudio Cambra, Frameworks 5.89)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Максим Маршев](https://t.me/msmarshev)  
_Источник:_ https://pointieststick.com/2021/12/03/this-week-in-kde-new-spectacle-features-and-tons-of-bugfixes/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: emoji → эмодзи -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
