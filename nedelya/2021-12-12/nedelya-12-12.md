# На этой неделе в KDE: полируем Ark и Dolphin

> 5–12 декабря, основное — прим. переводчика

## Wayland

В сеансе Plasma Wayland мышь и клавиатура [больше не «отваливаются» иногда после выключения и включения монитора](https://bugs.kde.org/show_bug.cgi?id=446699) (Xaver Hugl, Plasma 5.23.5)

В сеансе Wayland некоторые адаптированные к Wayland игры [снова открываются с правильным размером окон](https://bugs.kde.org/show_bug.cgi?id=444962) (Влад Загородний, Plasma 5.24)

В сеансе Wayland [курсоры теперь гладкие, а не пиксельные](https://invent.kde.org/plasma/kwin/-/merge_requests/1771), при использовании дробных значений масштабирования (Julius Zint, Plasma 5.24)

## Новые возможности

Утилита для создания снимков экранов Spectacle [научилась обрезать и масштабировать изображения, отменять и повторять действия, и пр.](https://invent.kde.org/graphics/spectacle/-/merge_requests/100) (Damir Porobic и Antonio Prcela, kImageAnnotator 0.6.0 или более поздних версий в Spectacle 22.04)

Виджет «Погода» теперь также может использовать [данные о погоде, предоставленные Метеорологической службой Германии (DWD)](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1212) (Emily Elhert, Plasma 5.24)

Теперь можно [менять размер окна калькулятора KCalc](https://bugs.kde.org/show_bug.cgi?id=441986) (Niklas Freund, KCalc 22.04)

## Исправления ошибок и улучшения производительности

Диспетчер файлов «Dolphin» [больше не падает, когда Ark создаёт архив формата .7z](https://bugs.kde.org/show_bug.cgi?id=443540) (Méven Car, Ark 21.12.1)

Действие «Упаковать» в контекстном меню диспетчера файлов [теперь учитывает пользовательскую настройку из Ark](https://invent.kde.org/utilities/ark/-/merge_requests/80): открывать ли новое окно файлового менеджера с архивом, когда операция завершится («2155X», Ark 22.04)

Параметры системы [больше не падают, когда вы используете окно «Загрузить оформления…» для изменения темы оформления рабочей среды](https://bugs.kde.org/show_bug.cgi?id=439797) (Alexander Lohnau, Plasma 5.23.5)

Исправлена ошибка с отрисовкой определённых типов кнопок оформления Breeze, [приводившая к падению некоторых приложений](https://invent.kde.org/plasma/breeze/-/merge_requests/163) (David Edmundson, Plasma 5.23.5)

[Действия](https://bugs.kde.org/show_bug.cgi?id=444365) и [запросы D-Bus](https://bugs.kde.org/show_bug.cgi?id=446441) к диспетчеру буфера обмена (Klipper) снова получают полный текст содержимого, а не его урезанную версию (David Edmundson и ValdikSS, Plasma 5.23.5)

Функция ограничения уровня заряда батареи [теперь поддерживает больше типов аккумуляторов](https://invent.kde.org/plasma/powerdevil/-/merge_requests/70) (Ian Douglas Scott и Méven Car, Plasma 5.24)

Ускорен [вывод содержимого очень больших папок](https://invent.kde.org/frameworks/kio/-/merge_requests/433) (Méven Car, Frameworks 5.90)

## Улучшения пользовательского интерфейса

Любые параметры комментирования в Spectacle, которые вы можете изменить, [теперь сохраняются между перезапусками приложения](https://bugs.kde.org/show_bug.cgi?id=436608) (Antonio Prcela, Spectacle 22.04)

Просмотрщик изображений Gwenview [теперь сглаживает изображения при масштабировании до 400%, после чего переключается на пикселизированное увеличение без сглаживания](https://bugs.kde.org/show_bug.cgi?id=443010) (Nate Graham, Gwenview 22.04)

При невозможности открыть файл в Dolphin (например, из-за его повреждения) [ошибка теперь показывается вверху основного окна, а не в отдельном диалоговом](https://invent.kde.org/system/dolphin/-/merge_requests/302). Также, не до конца загруженные и находящиеся в процессе обработки файлы с соответствующим расширением `.part` [не будут открываться, а сама попытка их открыть вызовет эту ошибку](https://invent.kde.org/frameworks/kio/-/merge_requests/652) (Kai Uwe Broulik, Dolphin 22.04 и Frameworks 5.90):

![0](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211210_113222.png)

Теперь диспетчер файлов можно [найти по запросам «Проводник» и «Finder»](https://invent.kde.org/system/dolphin/-/merge_requests/300) («tornado 99», Dolphin 22.04)

В Параметрах системы на странице «Настройка экранов» представление расположения экранов [теперь показывает серийные номера устройств, когда обнаружено несколько подключённых мониторов одной модели](https://bugs.kde.org/show_bug.cgi?id=446486), чтобы помочь вам отличить их друг от друга (Méven Car, Plasma 5.24)

Виджеты [теперь могут быть добавлены на рабочий стол одним нажатием на них в списке](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/734) (а не только перетаскиванием — прим. переводчика). Добавленные таким способом виджеты [будут размещены по центру экрана](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/408). (Arjen Hiemstra, Plasma 5.24 и Frameworks 5.90)

Центр приложений Discover [больше не показывает страшное предупреждение «Следующие пакеты будут удалены» для многоверсионных пакетов](https://invent.kde.org/plasma/discover/-/merge_requests/208), т.е. таких, которые могут иметь более одной установленной версии одновременно, и удаляемые пакеты попросту заменяются более новой версией себя же (Aleix Pol Gonzalez, Plasma 5.24)

Проверка звуковых устройств в Параметрах системы [теперь выглядит лучше](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/101) (Ismael Asensio, Plasma 5.24):

![3](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211208_110622.png)

Теперь вы можете [настроить больше восьми запасных раскладок клавиатуры](https://bugs.kde.org/show_bug.cgi?id=446456) (Андрей Бутырский, Plasma 5.24)

Discover в режиме просмотра подробностей [теперь отображает источник обновления каждого пакета](https://invent.kde.org/plasma/discover/-/merge_requests/217) (Ismael Asensio, Plasma 5.24):

![4](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211210_105045.png)

В меню запуска приложений строки на боковой панели [больше не имеют стрелочек. Это соответствует тому, как боковые панели обычно представлены в других местах](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/737) (Mikel Johnson, Plasma 5.24):

![6](https://pointieststick.files.wordpress.com/2021/12/no-arrows-in-kickoff.png)

Фон элементов панели задач в состояниях «активно» и «требует внимания» [был сделан более ярким и легко различимым](https://bugs.kde.org/show_bug.cgi?id=434821) (Frédéric Parrenin, Frameworks 5.90):

![7](https://pointieststick.files.wordpress.com/2021/12/tasksbar-saturated-after.png)

Типичные значки «диспетчер файлов» и «настройки» (используемые, в том числе, приложениями Dolphin и Параметры системы) [теперь подстраиваются под цвет выделения](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/192) (Артём Гринёв, Frameworks 5.90):

![8](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211210_203054.png)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Иван Ткаченко (ratijas)](https://t.me/ratijas)  
_Источник:_ https://pointieststick.com/2021/12/10/this-week-in-kde-polishing-up-ark-and-dolphin/  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: Klipper → диспетчер буфера обмена (Klipper) -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: file manager → диспетчер файлов -->
<!-- 💡: icon → значок -->
<!-- 💡: task manager → панель задач -->
