# На этой неделе в KDE: всего и побольше

В сегодняшнем выпуске будет что-нибудь для каждого! Заблаговременные праздничные подарки!

> 12–19 декабря, основное — прим. переводчика

## Новые возможности

Теперь вы можете [сменить обои](https://bugs.kde.org/show_bug.cgi?id=358038) на любое изображение с помощью его контекстного меню! (Fushan Wen, Plasma 5.24):

![0](https://pointieststick.files.wordpress.com/2021/12/screenshot_20211213_133907.png)

*Не волнуйтесь, это работает и в диспетчере файлов Dolphin!*

В Параметрах системы при использовании в сеансе Wayland [появилась](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/677) страница настроек графического планшета. Сейчас опций там не так много, но со временем будет всё больше (Aleix Pol Gonzalez, Plasma 5.24)

Теперь глобальные темы [могут устанавливать и изменять](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1243) макеты панели Latte (Michail Vourlakos, Plasma 5.24)

## Wayland

В сеансе Wayland функция автоматического копирования снимка экрана в буфер обмена теперь [работает](https://bugs.kde.org/show_bug.cgi?id=429390) при вызове программы Spectacle глобальной комбинацией клавиш (Méven Car, Spectacle 22.04)

В сеансе Wayland теперь [работают](https://bugs.kde.org/show_bug.cgi?id=427771) настройки мыши и сенсорной панели, позволяющие переключаться между «плоским» и «адаптивным» режимами ускорения (Arjen Hiemstra, Plasma 5.23.5)

В сеансе Wayland применение свойства окна «Скрыть заголовок и границы» больше [не делает](https://bugs.kde.org/show_bug.cgi?id=445140) окно суперкрошечным (Ismael Asensio, Plasma 5.23.5)

В сеансе Wayland различные веб-браузеры на основе Chromium теперь [правильно отображают](https://bugs.kde.org/show_bug.cgi?id=445259) свои окна (Влад Загородний, Plasma 5.24)

В сеансе Wayland теперь [можно использовать](https://bugs.kde.org/show_bug.cgi?id=397662) сочетание клавиш Meta+Tab для циклического переключения между более чем двумя комнатами Plasma (David Redondo, Plasma 5.24)

В сеансе Wayland перетаскивание теперь [работает](https://invent.kde.org/plasma/kwayland-server/-/merge_requests/333) в ОС FreeBSD (Влад Загородний, Plasma 5.24)

## Исправления ошибок и улучшения производительности

Параметры системы больше [не падают](https://bugs.kde.org/show_bug.cgi?id=439797) при попытке установить или обновить глобальные темы (David Edmundson, Plasma 5.23.5)

Изменение уровня масштабирования с помощью прокрутки соответствующего выпадающего списка в просмотрщике изображений Gwenview теперь [работает более предсказуемо и надёжно](https://invent.kde.org/graphics/gwenview/-/merge_requests/112) (Felix Ernst, Gwenview 21.12.1)

[Исправлена](https://invent.kde.org/network/kio-extras/-/merge_requests/136) утечка памяти в генераторе миниатюр для предварительного просмотра (Waqar Ahmed, kio-extras 22.04)

Производительность прокрутки в эмуляторе терминала Konsole [повысилась в 2 раза](https://invent.kde.org/utilities/konsole/-/merge_requests/555)! (Waqar Ahmed, Konsole 22.04)

[Исправлены](https://invent.kde.org/plasma/kwin/-/merge_requests/1791) различные утечки памяти, которые могли привести к сбою диспетчера окон KWin при открытии разнообразных сторонних приложений или вызове нового эффекта обзора (Влад Загородний, Plasma 5.23.5)

Поиск в меню запуска приложений больше [не ломается](https://bugs.kde.org/show_bug.cgi?id=443131), когда создано несколько его экземпляров (Noah Davis, Plasma 5.23.5)

Пункт контекстного меню панели задач «Переместить в комнату > Во всех комнатах» [снова работает](https://bugs.kde.org/show_bug.cgi?id=440496) (Fushan Wen, Plasma 5.24)

При повороте монитора, на котором отображается полноэкранное содержимое, теперь [обеспечивается его правильное размещение](https://invent.kde.org/plasma/kwin/-/merge_requests/1789) (Jiya Dong, Plasma 5.24)

Было [включено](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1242) первое из многих предстоящих исправлений для многомониторных конфигураций, которое должно помочь в решении проблемы перемешивания панелей и рабочих столов при удалении и повторном подключении экранов (Marco Martin, Plasma 5.24)

Скомпонованные кнопки в приложениях GTK, стилизованные под тему Breeze GTK, [теперь](https://bugs.kde.org/show_bug.cgi?id=446206) имеют целостное выпуклое оформление, подчёркивающее их связь (Jan Blackquill, Plasma 5.24):

![1](https://pointieststick.files.wordpress.com/2021/12/linked-buttons.png)

Некоторые всплывающие подсказки в виджетах Plasma больше [не содержат](https://bugs.kde.org/show_bug.cgi?id=442745) визуальных артефактов по углам при использовании темы Breeze (Noah Davis, Frameworks 5.90):

![2](https://pointieststick.files.wordpress.com/2021/12/no-glitch.png)

## Улучшения пользовательского интерфейса

Когда вы пытаетесь запустить мастер импорта с камеры Gwenview без необходимого пакета, программа теперь [определяет это и помогает установить его](https://bugs.kde.org/show_bug.cgi?id=446420) (Fushan Wen, Gwenview 22.04):

![3](https://pointieststick.files.wordpress.com/2021/12/gwenview-importer-message.png)

При использовании общесистемной настройки двойного щелчка в Dolphin теперь [можно](https://invent.kde.org/system/dolphin/-/merge_requests/286) дважды щёлкнуть по каталогу с зажатым Ctrl, чтобы открыть его в новой вкладке, а также дважды щёлкнуть с зажатым Shift, чтобы открыть его в новом окне (Alessio Bonfiglio, Dolphin 22.04)

Discover теперь [способен](https://bugs.kde.org/show_bug.cgi?id=445596) открывать и устанавливать локально загруженные приложения Flatpak из репозиториев, которые не активны в системе; при этом он сообщает, что в результате установки содержащий репозиторий будет добавлен (Aleix Pol Gonzalez, Plasma 5.24):

![4](https://pointieststick.files.wordpress.com/2021/12/discover-flatpak-message.png)

*Слишком большая пустота внизу сообщения — известная ошибка, которая рано или поздно будет исправлена*

Теперь [можно открыть](https://bugs.kde.org/show_bug.cgi?id=438213) программу Информация о системе с соответствующей страницы Параметров системы (Harald Sitter, Plasma 5.24):

![5](https://pointieststick.files.wordpress.com/2021/12/more-info.png)

Выгрузка изображений в Imgur теперь [показывает результат](https://bugs.kde.org/show_bug.cgi?id=394181) с помощью системного уведомления, а также отображает ссылку для удаления на случай ошибочной отправки (Nicolas Fella, Frameworks 5.90):

![7](https://pointieststick.files.wordpress.com/2021/12/cool-stuff.png)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Андреев](https://vk.com/ilyandl)  
_Источник:_ https://pointieststick.com/2021/12/17/this-week-in-kde-kind-of-everything/  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
