# На этой неделе в KDE: Finally root file operations in Dolphin

> 26 декабря – 2 января, основное — прим. переводчика

Happy new year, everyone!

This week the last piece of a major project almost five years in the making was merged: [Polkit support in KIO](https://bugs.kde.org/show_bug.cgi?id=179678)! This allows Dolphin and other KDE apps that use the KIO library to create, move, copy, trash, and delete files in non-user-owned locations! It took a long time, but we finally got it. Thanks very much to Jan Blackquill for pushing this over the finish line and Chinmoy Ranjan Pradhan for starting it and getting it very far those years ago. Support will arrive in Frameworks 5.91 in a little over a month. Until then, please test it in [KDE Neon Unstable](https://neon.kde.org/download) or [openSUSE Krypton](http://download.opensuse.org/repositories/KDE:/Medias/images/iso/) or your favorite distro’s «unstable» KDE packages, and [file bugs on `frameworks-kio`](https://bugs.kde.org/enter_bug.cgi?product=frameworks-kio) if things don’t work right!

## Другие новые возможности

Konsole [now lets you open the current directory or any other folder you right-click on in any app, not just the file manager](https://invent.kde.org/utilities/konsole/-/merge_requests/527) (Jan Blackquill, Konsole 22.04):

![0](https://pointieststick.files.wordpress.com/2021/12/open-with.png)

KRunner [now has an inline help feature, which you can show by clicking on a new question mark icon on its toolbar or typing «?»](https://bugs.kde.org/show_bug.cgi?id=433636) And while in Help mode, clicking a particular plugin will show you all the different search syntaxes for it! (Alexander Lohnau, Plasma 5.24)

* ![1](https://pointieststick.files.wordpress.com/2021/12/overview.png)

* ![2](https://pointieststick.files.wordpress.com/2021/12/details.png)

Users of the «Picture of the Day» wallpaper plugin [can now pull images from](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/87) <http://simonstalenhag.se>, which is full of cool and creepy sci-fi images (Алексей Андреев, Plasma 5.24)

## Wayland

In the Plasma Wayland session, [KWin now supports greater than 8-bit color](https://invent.kde.org/plasma/kwin/-/merge_requests/1641) (Xaver Hugl, Plasma 5.24)

In the Plasma Wayland session, [advanced keyboard options once again work properly](https://bugs.kde.org/show_bug.cgi?id=433265) (Fabian Vogt, Plasma 5.23.5)

In the Plasma Wayland session, System Settings [no longer crashes if you let the Display & Monitor’s revert timer get all the way down to 0 seconds](https://bugs.kde.org/show_bug.cgi?id=447199) (Méven Car, Plasma 5.24)

In the Plasma Wayland session, restoring a minimized or maximized window [now does what it does on X11](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/549): switches to the virtual desktop that the window was on before restoring it, instead of restoring it to the current virtual desktop (Alex Rosca, Plasma 5.24)

Scroll behavior in Qt Quick software [has been improved in several ways, including making the touchpad speed the same as it is in Qt Widgets scroll views, especially in the Plasma Wayland session when using screen scaling](https://invent.kde.org/frameworks/kirigami/-/merge_requests/415) (Noah Davis, Frameworks 5.90)

## Исправления ошибок и улучшения производительности

Gwenview [no longer sometimes crashes when opening JPEG files and the system’s `libexiv2` library is older than version 0.27.5](https://bugs.kde.org/show_bug.cgi?id=441121) (Lukáš Karas, Gwenview 21.12.1)

When Partition Manager is used to reformat a partition, [it is no longer owned by root by default](https://bugs.kde.org/show_bug.cgi?id=407192) (Tomaz Canabrava and Andrius Štikonas, Partition Manager 22.04)

The System Tray [now makes itself translucent or opaque based on the translucency/opacity setting of its parent panel, as expected](https://bugs.kde.org/show_bug.cgi?id=439025) (Konrad Materka, Plasma 5.23.5)

Window decoration themes with rounded corners [no longer suffer transparency and rotation-related related visual glitches when using a fractional scale factor like 125% or 150%](https://invent.kde.org/plasma/kwin/-/merge_requests/1805) (Julius Zint, Plasma 5.24)

Apps that update their window titles frequently [no longer cause Plasma to consume excessive CPU resources or hang](https://bugs.kde.org/show_bug.cgi?id=414121) (Fushan Wen, Plasma 5.24)

## Улучшения пользовательского интерфейса

In Dolphin, multi-line file/folder labels are now capped at 3 lines long by default, [and when you hover over them, you can see a tooltip that displays the full text](https://invent.kde.org/system/dolphin/-/merge_requests/312) (Leo Treloar, Dolphin 22.04):

![3](https://pointieststick.files.wordpress.com/2021/12/tooltip.png)

In the Task Manager’s context menu, the «Start New Instance» context item [has been renamed to «Open New Window» for clarity](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/133) and [no longer appears for apps marked as having a single main window](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1309) or that [already provide a «New Window» action of their own](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1312), and the «More Actions» item [has been moved to the bottom and renamed to just «More»](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/769) (Nicolas Fella and Nate Graham, Plasma 5.24):

* ![4](https://pointieststick.files.wordpress.com/2021/12/dolphin.png)

* ![5](https://pointieststick.files.wordpress.com/2021/12/konsole.png)

Discover [now has an option to automatically reboot after an update is complete](https://bugs.kde.org/show_bug.cgi?id=419504), which appears on the footer once the update process has begun (Aleix Pol Gonzalez, Plasma 5.24)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://pointieststick.com/2021/12/31/this-week-in-kde-finally-root-file-operations-in-dolphin/  

<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: directory → каталог -->
<!-- 💡: file manager → диспетчер файлов -->
<!-- 💡: icon → значок -->
<!-- 💡: locations → расположения -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
