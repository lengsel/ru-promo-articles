# Setting a default global theme has just become much easier

_Автор:_ [David Redondo](https://blog.david-redondo.de/about/)

**TL;DR:** Just set the default global theme in `/etc/xdg/kdeglobals`, and you're good to go.  
In the last years, System Settings has been gradually improved with each release featuring more pages being cleaned up, redesigned or even rewritten from scratch. In this process there was also focus on proper state handling, adding the ‘Defaults’ button where missing and sensible, adding highlighting of changed settings and making sure that these systems do the correct thing. In this post I will explain how Global Themes factor into this and how their pre-run set-up is now much simpler for distributions starting with Plasma 5.24.

## Global Themes as defaults supplier

**Disclaimer**: The changes described here were not done by me, but I wanted to give a full overview and context.

In the past, Global Themes and all the other settings were separate and parallel to each other. With the increasing roll-out of the «highlight non-default settings» feature, this could lead to confusing situations. For example, selecting Breeze Dark global theme, which applies the Breeze Dark color scheme, would mark the color scheme as «changed from default», and the «Defaults» button on the color scheme page would select Breeze Light again, even it is reasonable to expect that Breeze Dark should be selected when clicking the button which can be interpreted as «Revert manually changed settings».

![Appearance category in System Settings](https://blog.david-redondo.de/assets/appearance.png)

Another pain point was that it was not obvious which settings would be changed by applying a Global Theme. The pages in the Appearance category are now displayed indented under the Global Theme entry in the sidebar to visualize that, on one hand, a global theme encompasses these options, and on the other hand, illustrate the parent-child relationship between the pages.

This ties also nicely into the solution chosen for the first described issue. Starting with Plasma 5.22, setting defaults are taken from the active global theme. This means that when applying the Breeze Dark global theme, it will still change the color scheme to Breeze Dark, but will also change the default color scheme to Breeze Dark. To achieve this, Plasma prepends a directory `kdedefaults` to `$XDG_CONFIG_DIRS` (see [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)), which it creates in your `$XDG_CONFIG_HOME` (typically `~/.config`). This makes it the most important configuration location after your personal config folder. When applying a global theme, its settings will be applied and additionally written to files inside `kdedefaults`. This makes the config system treat them as defaults because they are inside the directory with the highest precedence of all `$XDG_CONFIG_DIRS`.

## Default Global Theme

Without doing any changes, Breeze is the default global theme. It has the rather uninteresting property that it sets most settings to their default value. So when it is the default, everything works as it should out of the box. Ideally it should be possible to select a new default global theme by simply setting the relevant key `LookAndFeelPackage` in `kdeglobals` in `$XDG_CONFIG_DIRS` (usually `/etc/xdg/kdeglobals`).

However, that had the slight problem that it did not quite work. When logging as a new user, the settings displayed the correct theme as active, but it was not actually applied. To fix this, it was needed to switch to another theme and go back again, or manually run `plasma-apply-lookandfeel`. This is, of course, because nothing actually applied the settings as described in the previous section, just the setting which theme *should* be active was changed.

The good news is that this is now fixed and works nicely in Plasma 5.24. During startup, Plasma now checks which global theme is supposed to be active and to which the settings inside the `kdedefaults` folder belong. If there’s a mismatch, this folder is updated. This way it’s ensured the defaults are correct and correspond to the currently active global theme. In addition, on a new user with an empty config, reading will fall back to the defaults and return the settings of the global theme!

## Conclusion

To reiterate: changing, setting the default global theme and making sure it’s applied on new users now only requires one thing: modifying the relevant configuration key — `LookAndFeelPackage` in the KDE group — in a `kdeglobals` file somewhere in `$XDG_CONFIG_DIRS`.

_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Источник:_ https://blog.david-redondo.de/kde/2021/12/17/global-themes.html  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: directory → каталог -->
<!-- 💡: location → расположение -->
