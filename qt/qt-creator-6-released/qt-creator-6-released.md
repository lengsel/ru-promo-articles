# Выпуск Qt Creator 6

_Автор:_ David Schulz, The Qt Company

Мы рады представить вам релиз среды разработки Qt Creator 6!

Здесь перечислены некоторые изменения и улучшения, внесённые в релиз. Для более детального списка изменений перейдите по [ссылке](https://code.qt.io/cgit/qt-creator/qt-creator.git/tree/dist/changes-6.0.0.md?h=6.0).

> 2 декабря — прим. переводчика

## Общие изменения

* Наши официальные сборки теперь основаны на Qt версии 6.2!

* Мы предоставляем универсальные сборки Intel+ARM для macOS.

* Мы переместили запуск внешних процессов, таких как инструменты сборки, `clang-tidy` и другие, в отдельный фоновый процесс. Это позволит избежать проблем в Linux, где ответвление процесса от большого приложения тратит больше ресурсов, чем ответвление от небольшого процесса.

## Редактор

* Наш текстовый редактор теперь поддерживает редактирование несколькими курсорами (Alt+ЛКМ):

[![FirstTry](https://www.qt.io/hs-fs/hubfs/FirstTry.gif?width=320&name=FirstTry.gif)](https://code.qt.io/cgit/qt-creator/qt-creator.git/tree/dist/changes-6.0.0.md?h=6.0)

* Модель кода C++ была обновлена до LLVM 13.

* Редактирование C++ с помощью `clangd` теперь полностью поддерживается, но всё ещё отключено по умолчанию. Включается оно в опциях, *C++* > *Clangd*. Более подробно можно почитать в статье в англоязычном блоге про [поддержку `clangd` в Qt Creator](https://www.qt.io/blog/qt-creator-and-clangd-an-introduction?hsLang=en).

* Интегрированный Qt Quick Designer теперь выключен по умолчанию. Qt Creator будет открывать файлы `.ui.qml` в Qt Design Studio. Это шаг в сторону интеграции рабочих процессов между Qt Design Studio и Qt Creator ([видео](https://www.youtube.com/watch?v=o3ESfuBOaiI)). Qt Quick Designer всё ещё на месте, вы можете включить его вручную, отметив плагин QmlDesigner в разделе Помощь > О плагинах.

## Проекты

* В контекстное меню дерева проекта добавлена опция *Показать в каталоге*.

* В расширенный поиск добавлен глобальный поиск по файлам во всех каталогах проекта, схожий с «Быстрым поиском».

* Для проектов CMake мы удалили отдельный узел дерева проектов «Заголовки» и улучшили поддержку заголовочных файлов, указанных среди исходников целей сборки. Предпочтительно добавлять заголовочные файлы в список файлов цели, так как это помогает самой среде Qt Creator и другим утилитам по типу Clazy делать всё без ошибок. Область просмотра *Файловая система* и возможности поиска могут быть использованы в других случаях. Подробности — в англоязычном блоге: [Qt Creator 6 — обновление CMake](https://www.qt.io/blog/qt-creator-6-cmake-update?hsLang=en).

* Улучшена поддержка сборки и запуска в контейнерах Docker. Всё больше и больше мест в Qt Creator принимают пути к файлам на удалённых расположениях. Экспериментальная поддержка теперь доступна и пользователям Windows.
* <!-- «удалённые файлы» слишком двусмысленно, так чуть лучше -->

## Получить Qt Creator 6

Версию с открытым исходным кодом можно скачать [тут](https://www.qt.io/offline-installers?hsLang=en), найти коммерчески лицензированные пакеты можно на [Qt Account Portal](https://login.qt.io/). Qt Creator 6 также доступен в виде обновления в онлайн-установщике. Пожалуйста, сообщайте о проблемах в [систему отслеживания ошибок](https://bugreports.qt.io/projects/QTCREATORBUG). Вы также можете связаться с нами в IRC, #qt-creator на [irc.libera.chat](https://web.libera.chat/), и [списке рассылки Qt Creator](http://lists.qt-project.org/mailman/listinfo/qt-creator).

Руководство по Qt Creator доступно в самом Qt Creator в [режиме помощи](https://doc-snapshots.qt.io/qtcreator-master/creator-help.html) или онлайн на [портале документации Qt](https://doc-snapshots.qt.io/qtcreator-master/index.html).

_Перевод:_ [Станислав Тротнер](https://t.me/phanteist)  
_Источник:_ https://www.qt.io/blog/qt-creator-6-released

<!-- 💡: bug tracker → система отслеживания ошибок -->
<!-- 💡: directories → каталоги -->
